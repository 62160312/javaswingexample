/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.swingcomponent;

import javax.swing.*;

/**
 *
 * @author ASUS
 */
public class JSliderExample extends JFrame {

    public JSliderExample() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        JSliderExample frame = new JSliderExample();
        frame.pack();
        frame.setVisible(true);
    }
}
