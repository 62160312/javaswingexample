/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.swingcomponent;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author ASUS
 */
public class JLabelExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Label Example");
        JLabel l1,l2;
        l1 = new JLabel("First Label.");
        l1.setBounds(50, 50,100, 30);
        l2 = new JLabel("Second Label.");
        l2.setBounds(50, 100,100, 30);
        frame.add(l1);frame.add(l2);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        
    }
    
}
